Source: asterisk-core-sounds
Section: comm
Priority: optional
Maintainer: Debian VoIP Team <pkg-voip-maintainers@lists.alioth.debian.org>
Uploaders: Tzafrir Cohen <tzafrir@debian.org>, Lionel Elie Mamane <lmamane@debian.org>, Jeremy Lainé <jeremy.laine@m4x.org>, Bernhard Schmidt <berni@debian.org>
Build-Depends: debhelper (>= 9)
Standards-Version: 3.9.8.0
Vcs-Git: https://salsa.debian.org/pkg-voip-team/asterisk-core-sounds.git
Vcs-Browser: https://salsa.debian.org/pkg-voip-team/asterisk-core-sounds

Package: asterisk-core-sounds-en
Architecture: all
Depends: ${misc:Depends}, asterisk-core-sounds-en-gsm | asterisk-core-sounds-en-g722 | asterisk-core-sounds-en-wav
Conflicts: asterisk-core-sounds-en-gsm ( << 1.4.21-2 )
Enhances: asterisk
Provides: asterisk-prompt-en, asterisk-prompt-en-us
Description: asterisk PBX sound files - US English
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 The core part of that collection in US English, by voice actress
 Allison Smith, is contained in various encodings in packages
 asterisk-core-sounds-en-*; this package registers these through the
 alternatives system to provide the default "en" (English) and "en_US"
 (USA English) sounds.

Package: asterisk-core-sounds-en-gsm
Architecture: all
Depends: ${misc:Depends}
Recommends: asterisk-core-sounds-en
Conflicts: asterisk-sounds-main
Replaces: asterisk-sounds-main
Description: asterisk PBX sound files - en-us/gsm
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 This package contains the core part of that collection in English (US,
 by Allison Smith) in raw gsm-fr format (Compressed. Takes relatively
 little space. playable with sox).

Package: asterisk-core-sounds-en-g722
Architecture: all
Depends: ${misc:Depends}
Recommends: asterisk-core-sounds-en
Description: asterisk PBX sound files - en-us/g722
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 This package contains the core part of that collection in English (US,
 by Allison Smith) in raw G.722 format (mildly compressed wide-band
 codec).

Package: asterisk-core-sounds-en-wav
Architecture: all
Depends: ${misc:Depends}
Recommends: asterisk-core-sounds-en
Description: asterisk PBX sound files - en-us/wav
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 This package contains the core part of that collection in English (US,
 by Allison Smith) in WAV format (8Khz, mono).

Package: asterisk-core-sounds-es
Architecture: all
Depends: ${misc:Depends}, asterisk-core-sounds-es-gsm | asterisk-core-sounds-es-g722 | asterisk-core-sounds-es-wav
Enhances: asterisk
Provides: asterisk-prompt-es, asterisk-prompt-es-mx
Description: asterisk PBX sound files - Spanish
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 The core part of that collection in Spanish, by voice actress Allison
 Smith, is contained in various encodings in packages
 asterisk-core-sounds-es-*; this package registers these through the
 alternatives system to provide the default "es" (Spanish) and "es_MX"
 (Mexican Spanish) sounds.

Package: asterisk-core-sounds-es-gsm
Architecture: all
Depends: ${misc:Depends}
Recommends: asterisk-core-sounds-es
Description: asterisk PBX sound files - es-mx/gsm
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 This package contains the core part of that collection in Spanish
 (MX, by Allison Smith) in raw gsm-fr format (Compressed. Takes
 relatively little space. playable with sox).

Package: asterisk-core-sounds-es-g722
Architecture: all
Depends: ${misc:Depends}
Recommends: asterisk-core-sounds-es
Description: asterisk PBX sound files - es-mx/g722
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 This package contains the core part of that collection in Spanish
 (MX, by Allison Smith) in raw G.722 format (mildly compressed
 wide-band codec).

Package: asterisk-core-sounds-es-wav
Architecture: all
Depends: ${misc:Depends}
Recommends: asterisk-core-sounds-es
Description: asterisk PBX sound files - es-mx/wav
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 This package contains the core part of that collection in Spanish
 (MX, by Allison Smith) in WAV format (8Khz, mono).

Package: asterisk-core-sounds-fr
Architecture: all
Depends: ${misc:Depends}, asterisk-core-sounds-fr-gsm | asterisk-core-sounds-fr-g722 | asterisk-core-sounds-fr-wav
Enhances: asterisk
Provides: asterisk-prompt-fr, asterisk-prompt-fr-ca
Description: asterisk PBX sound files - Canadian French
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 The core part of that collection in Canadian French, by voice actress
 June Wallack, is contained in various encodings in packages
 asterisk-core-sounds-fr-*; this package registers these through the
 alternatives system to provide the default "fr" (French) and "fr_CA"
 (Canadian French) sounds.

Package: asterisk-core-sounds-fr-gsm
Architecture: all
Depends: ${misc:Depends}
Recommends: asterisk-core-sounds-fr
Description: asterisk PBX sound files - fr-ca/gsm
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 This package contains the core part of that collection in French
 (Canadian, by June Wallack) in raw gsm-fr format (Compressed. Takes
 relatively little space. playable with sox).

Package: asterisk-core-sounds-fr-g722
Architecture: all
Depends: ${misc:Depends}
Recommends: asterisk-core-sounds-fr
Description: asterisk PBX sound files - fr-ca/g722
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 This package contains the core part of that collection in French
 (Canadian, by June Wallack) in raw G.722 format (mildly compressed
 wide-band codec).

Package: asterisk-core-sounds-fr-wav
Architecture: all
Depends: ${misc:Depends}
Recommends: asterisk-core-sounds-fr
Description: asterisk PBX sound files - fr-ca/wav
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 This package contains the core part of that collection in French
 (Canadian, by June Wallack) in WAV format (8Khz, mono).

Package: asterisk-core-sounds-it
Architecture: all
Depends: ${misc:Depends}, asterisk-core-sounds-it-gsm | asterisk-core-sounds-it-g722 | asterisk-core-sounds-it-wav
Enhances: asterisk
Provides: asterisk-prompt-it, asterisk-prompt-it-it
Description: asterisk PBX sound files - Italian
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 The core part of that collection in Italian, by voice actor Carlo
 Flora, is contained in various encodings in packages
 asterisk-core-sounds-it-*; this package registers these through the
 alternatives system to provide the default "it" and "it_IT" (Italian)
 sounds.

Package: asterisk-core-sounds-it-gsm
Architecture: all
Depends: ${misc:Depends}
Recommends: asterisk-core-sounds-it
Description: asterisk PBX sound files - it-it/gsm
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 This package contains the core part of that collection in Italian (by
 Carlo Flora) in raw gsm-fr format (Compressed. Takes relatively little
 space. playable with sox).

Package: asterisk-core-sounds-it-g722
Architecture: all
Depends: ${misc:Depends}
Recommends: asterisk-core-sounds-it
Description: asterisk PBX sound files - it-it/g722
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 This package contains the core part of that collection in Italian (by
 Carlo Flora) in raw G.722 format (mildly compressed wide-band codec).

Package: asterisk-core-sounds-it-wav
Architecture: all
Depends: ${misc:Depends}
Recommends: asterisk-core-sounds-it
Description: asterisk PBX sound files - it-it/wav
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 This package contains the core part of that collection in Italian (by
 Carlo Flora) in WAV format (8Khz, mono).

Package: asterisk-core-sounds-ru
Architecture: all
Depends: ${misc:Depends}, asterisk-core-sounds-ru-gsm | asterisk-core-sounds-ru-g722 | asterisk-core-sounds-ru-wav
Enhances: asterisk
Provides: asterisk-prompt-ru, asterisk-prompt-ru-ru
Description: asterisk PBX sound files - Russian
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 The core part of that collection in Russian, by Maxim Topal, is
 contained in various encodings in packages asterisk-core-sounds-ru-*;
 this package registers these through the alternatives system to
 provide the default "ru" and "ru_RU" (Russian) sounds.

Package: asterisk-core-sounds-ru-gsm
Architecture: all
Depends: ${misc:Depends}
Recommends: asterisk-core-sounds-ru
Description: asterisk PBX sound files - ru-ru/gsm
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 This package contains the core part of that collection in Russian
 (provided by Maxim Topal) in raw gsm-fr format (Compressed. Takes
 relatively little space. playable with sox).

Package: asterisk-core-sounds-ru-g722
Architecture: all
Depends: ${misc:Depends}
Recommends: asterisk-core-sounds-ru
Description: asterisk PBX sound files - ru-ru/g722
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 This package contains the core part of that collection in Russian
 (provided by Maxim Topal) in raw G.722 format (mildly compressed
 wide-band codec).

Package: asterisk-core-sounds-ru-wav
Architecture: all
Depends: ${misc:Depends}
Recommends: asterisk-core-sounds-ru
Description: asterisk PBX sound files - ru-ru/wav
 Asterisk is an Open Source PBX and telephony toolkit. It is, in a
 sense, middleware between Internet and telephony channels on the
 bottom, and Internet and telephony applications at the top.
 .
 Asterisk includes a set of standard sound files in various formats.
 This package contains the core part of that collection in Russian
 (provided by Maxim Topal) in WAV format (8Khz, mono).

